//
//  ViewController.swift
//  UIExample
//
//  Created by Ricardo López on 04/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var myImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myLabel.text = "Hola Mundo Cruel"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - User interaction
    
    @IBAction func leftButtonPressed(_ sender: Any) {
        myLabel.textAlignment = .left
    }
    
    @IBAction func centerButtonPressed(_ sender: Any) {
        myLabel.textAlignment = .center
    }
    
    @IBAction func rightButtonPressed(_ sender: Any) {
        myLabel.textAlignment = .right
    }
    
    @IBAction func redButtonPressed(_ sender: Any) {
        myLabel.textColor = .red
    }
    
    @IBAction func blackButtonPressed(_ sender: Any) {
        myLabel.textColor = .black
    }
    
    @IBAction func blueButtonPressed(_ sender: Any) {
        myLabel.textColor = .blue
    }
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        print(sender.value)
        myImageView.alpha = CGFloat(sender.value)
    }
}

