//
//  ListCollectionViewCell.swift
//  CollectionViewExample
//
//  Created by Ricardo López on 22/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import AlamofireImage

class ListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var product: ProductModel! {
        didSet {
            configureCell()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productImageView.image = nil
        productNameLabel.text = ""
        brandLabel.text = ""
        priceLabel.text = ""
    }
    
    // MARK: - Private methods
    
    private func configureCell() {
        
        if let firstImage = product.images.first {
            if let imageURL = URL(string: firstImage.path) {
                productImageView.af_setImage(withURL: imageURL, imageTransition: UIImageView.ImageTransition.crossDissolve(0.25))
            }
        }
        
        productNameLabel.text = product.data.name
        brandLabel.text = product.data.brand
        priceLabel.text = product.data.formattedPrice
    }
}
