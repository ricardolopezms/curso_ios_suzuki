//
//  Constants.swift
//  ParserExample
//
//  Created by Ricardo López on 13/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

struct Constants {
    
    struct API {
        static let xmlURLStr = "https://www.mobilestudio.mx/iphone/parser/books.xml"
        static let jsonURLStr = "https://www.mobilestudio.mx/iphone/parser/books.json"
    }
    
    struct Keys {
        static let book = "book"
        static let id = "id"
        static let author = "author"
        static let title = "title"
        static let genre = "genre"
        static let price = "price"
        static let publishDate = "publish_date"
        static let description = "description"
    }
}
