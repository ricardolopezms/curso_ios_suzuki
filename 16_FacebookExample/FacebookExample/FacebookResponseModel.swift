//
//  FacebookResponseModel.swift
//  FacebookExample
//
//  Created by Ricardo López on 21/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class FacebookResponseModel: Codable {
    var id: String
    var name: String
    var picture: FacebookPhotoResponseModel
}

class FacebookPhotoResponseModel: Codable {
    var data: FacebookPhotoDataModel
}

class FacebookPhotoDataModel: Codable {
    var url: String
}
