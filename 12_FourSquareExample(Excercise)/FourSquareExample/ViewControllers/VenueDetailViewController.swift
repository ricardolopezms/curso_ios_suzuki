//
//  VenueDetailViewController.swift
//  FourSquareExample
//
//  Created by Ricardo López on 19/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import MapKit

class VenueDetailViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var instructionsLabel: UILabel!
    
    var venueAnnotation: MKAnnotation!
    var userAnnotation: MKAnnotation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print("Venue annotation: \(venueAnnotation.title ?? "-")")
//        print("\tLat: \(venueAnnotation.coordinate.latitude)")
//        print("\tLon: \(venueAnnotation.coordinate.longitude)")
        
        mapView.addAnnotations([venueAnnotation, userAnnotation])
        mapView.showAnnotations([venueAnnotation, userAnnotation], animated: false)
        loadRoute()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private methods
    
    private func loadRoute() {
    
        let placeMark = MKPlacemark(coordinate: venueAnnotation.coordinate)
        let destinationMapItem = MKMapItem(placemark: placeMark)
        
        let request = MKDirectionsRequest()
        request.transportType = .walking
        request.requestsAlternateRoutes = false
        request.source = MKMapItem.forCurrentLocation()
        request.destination = destinationMapItem
        
        let directions = MKDirections(request: request)
        directions.calculate { (response, error) in
            
            if let error = error {
                print("Directions error: \(error.localizedDescription)")
            }
            else {
                if let routes = response?.routes {
                    for route in routes {
                        var instructionsStr = ""
                        for step in route.steps {
                            print(step.instructions)
                            instructionsStr += "\(step.instructions)\n"
                        }
                        self.instructionsLabel.text = instructionsStr
                        // TODO: Agregar la ruta al mapa
                        self.mapView.add(route.polyline, level: .aboveRoads)
                    }
                }
            }
        }
    }

}

// MARK: - MKMapViewDelegate

extension VenueDetailViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .red
        renderer.lineWidth = 5
        renderer.alpha = 0.75
        return renderer
    }
}


