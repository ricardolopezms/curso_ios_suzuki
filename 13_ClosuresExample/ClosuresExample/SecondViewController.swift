//
//  SecondViewController.swift
//  ClosuresExample
//
//  Created by Ricardo López on 19/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

protocol SecondViewDelegate: class {
    func secondViewDidSend(text: String)
}

class SecondViewController: UIViewController {
    
    weak var delegate: SecondViewDelegate?
    var myClosure: ((String) -> Void)?
    private var myCoolClosure: ((String, UIColor) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func set(myCoolClosure: @escaping ((String, UIColor) -> Void)) {
        self.myCoolClosure = myCoolClosure
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - User interaction
    
    @IBAction func delegateButtonPressed(_ sender: Any) {
        delegate?.secondViewDidSend(text: "Texto enviado por delegado :)")
    }
    
    @IBAction func closureButtonPressed(_ sender: Any) {
        //myClosure?("Texto enviado por closure")
        myCoolClosure?("EL TEXTO!", UIColor.red)
    }
}
