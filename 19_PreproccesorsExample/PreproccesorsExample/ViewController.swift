//
//  ViewController.swift
//  PreproccesorsExample
//
//  Created by Ricardo López on 26/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("URL: \(Constants.API.baseURL)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

struct Constants {
    
    struct API {
        #if SANDBOX
        static let baseURL = "https://sandbox.server.com"
        #else
        static let baseURL = "https://prod.server.com"
        #endif
    }
}
