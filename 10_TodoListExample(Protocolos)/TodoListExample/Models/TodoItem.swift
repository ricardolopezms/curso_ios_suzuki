//
//  TodoItem.swift
//  TodoListExample
//
//  Created by Ricardo López on 11/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation
import UIKit

class TodoItem {
    
    var title = ""
    var notes = ""
    var done = false
    var color = UIColor.lightGray
    
    init(title: String, notes: String, done: Bool, color: UIColor = .lightGray) {
        self.title = title
        self.notes = notes
        self.done = done
        self.color = color
    }
}
