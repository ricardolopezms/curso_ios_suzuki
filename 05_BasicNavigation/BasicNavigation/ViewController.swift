//
//  ViewController.swift
//  BasicNavigation
//
//  Created by Ricardo López on 05/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("ViewDidLoad ViewController")
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("ViewWillAppear ViewController")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("ViewDidAppear ViewController")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("didReceiveMemoryWarning ViewController")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("ViewWillDisappear ViewController")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("ViewDidDisappear ViewController")
    }

    // MARK: - User interaction

    @IBAction func showWithSegueIdButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "RedViewController", sender: nil)
    }

    
    @IBAction func showViewControllerWithVCIdButtonPressed(_ sender: Any) {
        if let blueViewController = storyboard?.instantiateViewController(withIdentifier: "BlueViewController") {
            navigationController?.show(blueViewController, sender: nil)
        }
    }
}

