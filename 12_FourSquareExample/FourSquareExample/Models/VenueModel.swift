//
//  VenueModel.swift
//  FourSquareExample
//
//  Created by Ricardo López on 15/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class VenueModel: Codable {
    
    var name: String
    var location: LocationModel
}
