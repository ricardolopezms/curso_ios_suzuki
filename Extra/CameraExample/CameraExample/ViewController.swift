//
//  ViewController.swift
//  CameraExample
//
//  Created by Ricardo López on 22/01/18.
//  Copyright © 2018 Ricardo López. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Mi App", message: "Selecciona una fuente", preferredStyle: .actionSheet)
        
        let albumAction = UIAlertAction(title: "Fotos", style: .default) { (action) in
            self.showPickerWith(source: .photoLibrary)
        }
        alertController.addAction(albumAction)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraAction = UIAlertAction(title: "Cámara", style: .default) { (action) in
                self.showPickerWith(source: .camera)
            }
            alertController.addAction(cameraAction)
        }
        present(alertController, animated: true, completion: nil)
    }
    
    private func showPickerWith(source: UIImagePickerControllerSourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        imagePickerController.sourceType = source
        present(imagePickerController, animated: true, completion: nil)
    }
}

// MARK: - UINavigationControllerDelegate, UIImagePickerControllerDelegate

extension ViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let selectedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            if let imageData = UIImageJPEGRepresentation(selectedImage, 0.5) {
                let string = imageData.base64EncodedString()
                print("BASE 64: \(string)")
            }
            imageView.image = selectedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}











