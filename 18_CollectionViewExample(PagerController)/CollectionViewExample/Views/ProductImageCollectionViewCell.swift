//
//  ProductImageCollectionViewCell.swift
//  CollectionViewExample
//
//  Created by Ricardo López on 25/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import AlamofireImage

class ProductImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    var productImage: ProductImageModel! {
        didSet {
            configureCell()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.image = nil
    }
    
    // MARK: - Private methods
    
    private func configureCell() {
        if let imageURL = URL(string: productImage.path) {
            photoImageView.af_setImage(withURL: imageURL)
        }
    }
}
