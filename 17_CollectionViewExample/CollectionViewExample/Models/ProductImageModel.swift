//
//  ProductImageModel.swift
//  CollectionViewExample
//
//  Created by Ricardo López on 18/01/18.
//  Copyright © 2018 Ricardo López. All rights reserved.
//

import Foundation

class ProductImageModel: Codable {
    
    var width: String
    var height: String
    var path: String
    var format: String
}
