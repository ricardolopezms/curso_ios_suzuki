//
//  ViewController.swift
//  ClosuresExample
//
//  Created by Ricardo López on 19/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Definición implícita => "() -> Void"
        let simpleClosure = {
            print("Hola mundo closures")
        }
        simpleClosure()
        
        let simpleCosureWithDefinition: () -> Void = {
            print("Hola mundo closures dos")
        }
        simpleCosureWithDefinition()
        
        // Closure con parámetros implícito
        let closureWithParams = { (numA: Int, numB: Int) in
            let sum = numA + numB
            print("Suma en closure: \(sum)")
        }
        closureWithParams(4, 5)
        
        // Closure con parámetros explícito
        let closureWithParamsAndDefinition: (Int, Int) -> Int = { (numA, numB) -> Int in
            let sum = numA + numA
            return sum
        }
        
        let sum = closureWithParamsAndDefinition(2, 5)
        print("SUM: \(sum)")
        
        let myClosure: (String, Int) -> Void = { (name, age) in
            print("Hola, me llamo \(name) y tengo \(age) años")
        }
        myClosure("Ricardo", 30)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SecondViewController" {
            // Asignamos el delegado
            let secondViewController = segue.destination as! SecondViewController
            secondViewController.delegate = self
            
            // Asignamos el closure
            /*
            secondViewController.myClosure = { (text) in
                print("Texto en closure: \(text)")
            }
            */
            
            secondViewController.set(myCoolClosure: { [unowned self] (text, color) in
                self.view.backgroundColor = color
                self.navigationController?.popViewController(animated: true)
            })
            
//            secondViewController.set(myCoolClosure: { [weak self] (text, color) in
//                self?.view.backgroundColor = color
//            })
        }
    }
}

// MARK: - SecondViewDelegate

extension ViewController: SecondViewDelegate {
    
    func secondViewDidSend(text: String) {
        print("Texto recibido en PRIMER VIEW: \(text)")
    }
}







