//
//  ViewController.swift
//  Calculadora
//
//  Created by Ricardo López on 05/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var displayLabel: UILabel!
    
    private var auxNumber = 0
    private var operation = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - User interaction
    
    @IBAction func numberButtonPressed(_ sender: UIButton) {
        print("Número: \(sender.tag)")
        
        if displayLabel.text == "0" {
            displayLabel.text = "\(sender.tag)"
        }
        else {
            if let currentNumber = displayLabel.text {
                displayLabel.text = "\(currentNumber)\(sender.tag)"
            }
        }
    }
    
    @IBAction func operationButtonPressed(_ sender: UIButton) {
        
        // 0 -> =
        // 1 -> +
        // 2 -> -
        // 3 -> X
        // 4 -> /
        
        if let currentDisplayNumberString = displayLabel.text {
            if let currentDisplayNumber = Int(currentDisplayNumberString) {
                
                switch sender.tag {
                case 0:
                    // TODO: Terminar la operación elegida
                    print("Mostrar resultado")
                    finishOperation(number: currentDisplayNumber)
                case 1, 2, 3, 4:
                    auxNumber = currentDisplayNumber
                    print("Número almacenado: \(auxNumber)")
                    displayLabel.text = "0"
                    operation = sender.tag
                default:
                    print("Nothing to do here :(")
                }
            }
        }
    }
    
    @IBAction func clearButtonPressed(_ sender: Any) {
        
    }
    
    private func finishOperation(number: Int) {
        
        var result = 0
        
        switch operation {
        case 1:
            result = auxNumber + number
        case 2:
            result = auxNumber - number
        case 3:
            result = auxNumber * number
        case 4:
            if number != 0 {
                result = auxNumber / number
            }
            else {
                let alertController = UIAlertController(title: "Oups!", message: "No se puede dividir entre cero", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    print("El usuario presionó OK")
                })
                alertController.addAction(okAction)
                present(alertController, animated: true, completion: nil)
            }
        default:
            print("Nothing to do here :(")
        }
        
        displayLabel.text = "\(result)"
    }
}

