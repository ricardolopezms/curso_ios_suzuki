//
//  ViewControllerUnitTests.swift
//  UnitTestExampleTests
//
//  Created by Ricardo López on 26/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import XCTest
@testable import UnitTestExample
import Alamofire

class ViewControllerUnitTests: XCTestCase {
    
    var viewController: ViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        viewController = storyboard.instantiateInitialViewController() as! ViewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            let _ = viewController.factorial(num: 4)
        }
    }
    
    func testSomeAsserts() {
        XCTAssertTrue(true)
        XCTAssertTrue(2 == 2)
        
        let view: UIView! = nil
        //view = UIView()
        
        XCTAssertNil(view, "El view NO es nil")
    }
    
    func testHelloWorld() {
        let result = viewController.helloWorld()
        XCTAssert(result == "Hello world!", "Hello world func failed on ViewController")
    }
    
    func testFactorial() {
        let result = viewController.factorial(num: 4)
        XCTAssert(result == 24, "Factorial func failed on ViewController")
    }
    
    func testAPICall() {
        
        let testExpectation = expectation(description: "TestAPICall")
        let booksURL = "https://www.mobilestudio.mx/iphone/parser/books.json"
        
        Alamofire.request(booksURL)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let value):
                    
                    let booksResponse = try! JSONDecoder().decode(BooksResponseModel.self, from: value)
                    XCTAssert(booksResponse.catalog.books.count == 12, "JSON no tiene todos los libros")
                    testExpectation.fulfill()
                case .failure(let error):
                    XCTFail("API call error: \(error.localizedDescription)")
                }
        }
     
        waitForExpectations(timeout: 3) { (error) in
            if let error = error {
                XCTFail("Failed: \(error.localizedDescription)")
            }
        }
    }
}





