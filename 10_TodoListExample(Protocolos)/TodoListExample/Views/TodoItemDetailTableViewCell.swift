//
//  TodoItemDetailTableViewCell.swift
//  TodoListExample
//
//  Created by Ricardo López on 11/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class TodoItemDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var todoTitleLabel: UILabel!
    @IBOutlet weak var todoNotesLabel: UILabel!
    @IBOutlet weak var colorIndicatorView: UIView!
    
    var todoItem: TodoItem! {
        didSet {
            configureCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private methods
    
    private func configureCell() {
        
        colorIndicatorView.backgroundColor = todoItem.color
        if todoItem.done {
            let attributesDict: [NSAttributedStringKey: Any] = [
                NSAttributedStringKey.strikethroughStyle : 1
            ]
            let attributedString = NSAttributedString(string: todoItem.title, attributes: attributesDict)
            todoTitleLabel.attributedText = attributedString
        }
        else {
            todoTitleLabel.text = todoItem.title
        }
        todoNotesLabel.text = todoItem.notes
    }
}
