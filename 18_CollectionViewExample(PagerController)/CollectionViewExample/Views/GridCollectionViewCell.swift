//
//  GridCollectionViewCell.swift
//  CollectionViewExample
//
//  Created by Ricardo López on 25/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import AlamofireImage

class GridCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    
    var product: ProductModel! {
        didSet {
            configureCell()
        }
    }
    
    // MARK: - Private methods
    
    private func configureCell() {
        productNameLabel.text = product.data.name
        if let firstImage = product.images.first, let imageURL = URL(string: firstImage.path) {
            productImageView.af_setImage(withURL: imageURL, imageTransition: UIImageView.ImageTransition.crossDissolve(0.25))
        }
    }
}
