//
//  TodoListViewController.swift
//  TodoListExample
//
//  Created by Ricardo López on 11/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class TodoListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource: [TodoItem] = []
    private var isDetailActive = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let todosDict = readJsonToDict(fileName: "todos.json") {
            print("Dict: \(todosDict)")
            if let todosArray = todosDict["todos"] as? [[String: Any]] {
                print("Todos: \(todosArray.count)")
                for todoDict in todosArray {
                    if let title = todoDict["title"] as? String, let notes = todoDict["notes"] as? String, let done = todoDict["done"] as? Bool {
                        // TODO: Create todo
                        print("\(title) -> \(notes)")
                        let todoItem = TodoItem(title: title, notes: notes, done: done)
                        dataSource.append(todoItem)
                    }
                }
                print(dataSource)
            }
        }
        else {
            print("No existe el archivo, creamos la data...")
            createDummyData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isDetailActive = UserDefaults.standard.bool(forKey: "DETAIL_ACTIVE")
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreateTodoViewController" {
            // 4
            let createTodoViewController = segue.destination as! CreateTodoViewController
            createTodoViewController.delegate = self
        }
    }
    
    // MARK: - Private methods
    
    private func createDummyData() {
        
        let todoDict: [String: Any] = [
            "title" : "Estudiar Swift",
            "notes" : "Buscar ejemplos de Swift y repasarlos",
            "done" : false
        ]
        
        let todoDict2: [String: Any] = [
            "title" : "Comprar barritas",
            "notes" : "Barritas de fruta para el desayuno",
            "done" : true
        ]
        
        let todoDict3: [String: Any] = [
            "title" : "Comprar el pizarrón",
            "notes" : "Buscar un pizarrón portatil para el curso",
            "done" : false
        ]
        
        let todos = [
            "todos" : [todoDict, todoDict2, todoDict3]
        ]
        
        writeDictToJson(dict: todos, fileName: "todos.json")
    }
    
    private func writeDictToJson(dict: [String: Any], fileName: String) {
        
        let data = try! JSONSerialization.data(withJSONObject: dict, options: [.prettyPrinted])
        let jsonString = String(data: data, encoding: .utf8)
        //print("JSON: \(jsonString)")
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let completePath = paths[0].appending("/\(fileName)")
        print(completePath)
        
        do {
            try jsonString?.write(toFile: completePath, atomically: true, encoding: .utf8)
        }
        catch let error {
            print("Write error: \(error.localizedDescription)")
        }
    }
    
    private func readJsonToDict(fileName: String) -> [String: Any]? {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let completePath = paths[0].appending("/\(fileName)")
        
        var dict: [String: Any]?
        
        do {
            let jsonString = try String(contentsOfFile: completePath, encoding: .utf8)
            
            if let data = jsonString.data(using: .utf8) {
                dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
        }
        catch let error {
            print("Reading error: \(error.localizedDescription)")
        }
        
        return dict
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension TodoListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isDetailActive {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TodoItemDetailTableViewCell") as! TodoItemDetailTableViewCell
            cell.todoItem = dataSource[indexPath.row]
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TodoItemTableViewCell") as! TodoItemTableViewCell
            cell.todoItem = dataSource[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - CreateTodoDelegate

extension TodoListViewController: CreateTodoDelegate {
    
    func createTodoDidCreate(todoItem: TodoItem) {
        // Aquí es donde recibimos el Todo creado en la pantalla de creación
        print("Todo recibido: \(todoItem.title)")
        dataSource.append(todoItem)
        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
}




