//
//  Book.swift
//  ParserExample
//
//  Created by Ricardo López on 13/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class Book: Codable {
    
    var id = ""
    var author = ""
    var title = ""
    var genre = ""
    var price = ""
    var publishDate = ""
    var bookDescription = ""
    
    // Para que la clase obtenga los datos desde el JSON, es necesario declararlos en el enum aunque la llave se llame igual que la variable, en caso de que las llaves coincidan al 100% con las variables, no declaramos el enum
    enum CodingKeys: String, CodingKey {
        case id
        case author
        case title
        case genre
        case price
        case publishDate = "publish_date"
        case bookDescription = "description"
    }
}


