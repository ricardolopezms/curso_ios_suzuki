//
//  DeepLinkingManager.swift
//  PushNotificationsExample
//
//  Created by Ricardo López on 27/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class DeepLinkingManager {
    
    static let sharedInstance = DeepLinkingManager()
    
    func handleURL(url: URL) {
        
        if let scheme = url.scheme, scheme == "appricardo" {
            if let host = url.host {
                switch host {
                case "v":
                    print("Go to color ViewController")
                case "p":
                    print("Go to product detail")
                case "d":
                    print(" Go to document viewcontroller")
                    let path = url.path
                    print(path)
                    let pathComponents = path.components(separatedBy: "/")
                    print(pathComponents)
                    let params = [
                        "document_type" : pathComponents[0],
                        "folio" : pathComponents[1]
                    ]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OPEN_DOCUMENT"), object: params)
                default:
                    print("We can't manage this URL")
                }
            }
        }
    }
}
