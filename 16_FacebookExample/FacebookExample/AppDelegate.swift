//
//  AppDelegate.swift
//  FacebookExample
//
//  Created by Ricardo López on 21/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import FacebookCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        NotificationCenter.default.addObserver(self, selector: #selector(showLoginScreen), name: NSNotification.Name(rawValue: "SHOW_LOGIN_SCREEN"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showProfileScreen), name: NSNotification.Name(rawValue: "SHOW_PROFILE_SCREEN"), object: nil)
        
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        if let _ = AccessToken.current {
            print("User logged in with Facebook")
            showProfileScreen()
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let bool = SDKApplicationDelegate.shared.application(app, open: url, options: options)
        return bool
    }
}

// MARK: - Change App rootViewController

extension AppDelegate {
    
    @objc private func showLoginScreen() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController")
        window?.rootViewController = viewController
    }
    
    @objc private func showProfileScreen() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let userProfileViewController = mainStoryboard.instantiateViewController(withIdentifier: "UserProfileViewController")
        window?.rootViewController = userProfileViewController
    }
}





