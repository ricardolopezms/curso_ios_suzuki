//
//  ProductDataModel.swift
//  CollectionViewExample
//
//  Created by Ricardo López on 18/01/18.
//  Copyright © 2018 Ricardo López. All rights reserved.
//

import Foundation

class ProductDataModel: Codable {
    
    var productDescription: String
    var price: String
    var sku: String
    var url: String
    var name: String
    var brand: String
    
    enum CodingKeys: String, CodingKey {
        case productDescription = "description"
        case price
        case sku
        case url
        case name
        case brand
    }
}

extension ProductDataModel {
    
    var formattedPrice: String {
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = "¥"
        numberFormatter.currencyDecimalSeparator = "."
        numberFormatter.currencyGroupingSeparator = ","
        
        if let doublePrice = Double(price) {
            let numberPrice = NSNumber(value: doublePrice)
            return numberFormatter.string(from: numberPrice) ?? "$\(price)"
        }
        
        return "$\(price)"
    }
}


