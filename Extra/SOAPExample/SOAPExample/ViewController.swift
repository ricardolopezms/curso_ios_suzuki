//
//  ViewController.swift
//  SOAPExample
//
//  Created by Ricardo López on 15/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //loadSOAP()
        //loadSOAPV2()
        loadSOAPV3()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Private methods
    
    // http://189.206.77.234:88/WSLogica/WSLogicaDMS.asmx?op=ValidarCampanias
    
    private func loadSOAP() {
        
        let vin = "JS1GT78A5A2100381"
        let dealer = "200055"
        
        let xmlRequestBody = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
        "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
        "<soap:Body>" +
        "<ValidarCampanias xmlns=\"http://suzuki.com.mx/\">" +
        "<_vin>\(vin)</_vin>" +
        "<_idDealer>\(dealer)</_idDealer>" +
        "</ValidarCampanias>" +
        "</soap:Body>" +
        "</soap:Envelope>"

        let apiURL = "http://189.206.77.234:88/WSLogica/WSLogicaDMS.asmx?WSDL"
        
        let url = URL(string: apiURL)!
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("text/xml", forHTTPHeaderField: "Content-Type")
        
        if let bodyData = xmlRequestBody.data(using: .utf8) {
            urlRequest.httpBody = bodyData
        }
        
        Alamofire.request(urlRequest).validate().responseString { (response) in
            switch response.result {
            case .success(let value):
                print("SOAP Response: \(value)")
            case .failure(let error):
                print("SOAP Response error: \(error.localizedDescription)")
            }
        }
    }
    
    
    private func loadSOAPV2() {
        let vin = "JS1GT78A5A2100381"
        let dealer = "200055"
        let validateCampaniaRequest = ValidarCampaniasRequestModel(dealer: dealer, vin: vin)
        
        Alamofire.request(CampaniasRouter.validarCampania(model: validateCampaniaRequest)).validate().responseString { (response) in
            switch response.result {
            case .success(let value):
                print("SOAP V2 Response: \(value)")
            case .failure(let error):
                print("SOAP V2 Response error: \(error.localizedDescription)")
            }
        }
    }
    
    private func loadSOAPV3() {
        
        let properties = [
            "_vin" : "JS1GT78A5A2100381",
            "_idDealer" : "200055"
        ]
        
        let generalRequest = GeneralRequestModel(operationName: "ValidarCampanias", properties: properties)
        
        Alamofire.request(CampaniasRouter.validarCampaniaGR(model: generalRequest)).validate().responseString { (response) in
            switch response.result {
            case .success(let value):
                print("SOAP V2 Response: \(value)")
            case .failure(let error):
                print("SOAP V2 Response error: \(error.localizedDescription)")
            }
        }
    }
}









