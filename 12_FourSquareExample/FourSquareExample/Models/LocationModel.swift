//
//  LocationModel.swift
//  FourSquareExample
//
//  Created by Ricardo López on 15/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class LocationModel: Codable {
    
    var latitude: Double
    var longitude: Double
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
}
