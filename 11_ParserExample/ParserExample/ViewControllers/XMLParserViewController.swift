//
//  XMLParserViewController.swift
//  ParserExample
//
//  Created by Ricardo López on 13/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire

class XMLParserViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource: [Book] = []
    private var auxBook: Book!
    private var auxString = ""
    private var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl.addTarget(self, action: #selector(loadBooks), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        loadBooks()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private methods
    
    @objc private func loadBooks() {
        
        Alamofire.request(Constants.API.xmlURLStr)
            .validate()
            .responseData { (response) in
            
                switch response.result {
                case .success(let value):
                    
                    self.dataSource.removeAll()
                    
                    print("Data: \(value)")
                    let parser = XMLParser(data: value)
                    parser.delegate = self
                    parser.parse()
                    
                    print("Libros parseados: \(self.dataSource.count)")
                    for book in self.dataSource {
                        print(book.title)
                        print("\t\(book.id)")
                        print("\t\(book.author)")
                        print("\t\(book.title)")
                        print("\t\(book.price)")
                        print("\t\(book.publishDate)")
                        print("\t\(book.genre)")
                        print("\t\(book.bookDescription)")
                    }
                    self.tableView.reloadData()
                    
                case .failure(let error):
                    print("XML request error: \(error.localizedDescription)")
                }
                self.refreshControl.endRefreshing()
        }
    }
}

// MARK: - XMLParserDelegate

extension XMLParserViewController: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        print("Abre: \(elementName)")
        // print("ATTR: \(attributeDict)")
        if elementName == Constants.Keys.book {
            auxBook = Book()
            if let bookId = attributeDict[Constants.Keys.id] {
                auxBook.id = bookId
            }
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        print("Caracteres: \(string)")
        auxString = string
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        print("Cierra: \(elementName)")
        switch elementName {
        case Constants.Keys.author:
            auxBook.author = auxString
        case Constants.Keys.title:
            auxBook.title = auxString
        case Constants.Keys.price:
            auxBook.price = auxString
        case Constants.Keys.publishDate:
            auxBook.publishDate = auxString
        case Constants.Keys.genre:
            auxBook.genre = auxString
        case Constants.Keys.description:
            auxBook.bookDescription = auxString
        case Constants.Keys.book:
            dataSource.append(auxBook)
        default:
            print("Nothing to do here...")
        }
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension XMLParserViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "XMLBookTableViewCell") as! XMLBookTableViewCell
        cell.book = dataSource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Book selected")
    }
}



