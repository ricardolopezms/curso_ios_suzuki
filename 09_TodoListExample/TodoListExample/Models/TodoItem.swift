//
//  TodoItem.swift
//  TodoListExample
//
//  Created by Ricardo López on 11/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation

class TodoItem {
    
    var title = ""
    var notes = ""
    var done = false
    
    init(title: String, notes: String, done: Bool) {
        self.title = title
        self.notes = notes
        self.done = done
    }
}
