//
//  ViewController.swift
//  Threads
//
//  Created by Ricardo López on 10/12/17.
//  Copyright © 2017 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var firstWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var secondWidthContraint: NSLayoutConstraint!
    @IBOutlet weak var thirdWidthContraint: NSLayoutConstraint!
    
    private var containerWidth: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        firstWidthConstraint.constant = 0
        secondWidthContraint.constant = 0
        thirdWidthContraint.constant = 0
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        containerWidth = containerView.frame.size.width
        
//        DispatchQueue.global(qos: .background).async {
//            sleep(2)
//            DispatchQueue.main.async {
//                //self.firstWidthConstraint.constant = self.containerWidth
//                self.secondWidthContraint.constant = self.containerWidth
//                self.thirdWidthContraint.constant = self.containerWidth
//                UIView.animate(withDuration: 0.5, animations: {
//                    self.view.layoutIfNeeded()
//                })
//            }
//        }
        
        
        // Sistema
        
//        DispatchQueue.global(qos: .background).async {
//            self.updateFirst()
//        }
//
//        DispatchQueue.global(qos: .background).async {
//            self.updateSecond()
//        }
//
//        DispatchQueue.global(qos: .background).async {
//            self.updateThird()
//        }
        
        let concurrentQueue = DispatchQueue(label: "com.app.myConcurrentQueue", qos: .background, attributes: .concurrent)
        //let concurrentQueue = DispatchQueue(label: "com.app.myConcurrentQueue", qos: .background)
        
        concurrentQueue.async {
            self.updateFirst()
        }
        
        concurrentQueue.async {
            self.updateSecond()
        }
        
        concurrentQueue.async {
            self.updateThird()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func updateFirst() {
        for x in 1...10 {
            let percentage = Double(x) / 10.0
            let width = CGFloat(percentage) * containerWidth
            DispatchQueue.main.async {
                self.firstWidthConstraint.constant = width
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.layoutIfNeeded()
                })
            }
            let delay = arc4random_uniform(2) + 1
            print("First delay: \(delay)")
            sleep(delay)
        }
    }
    
    private func updateSecond() {
        for x in 1...10 {
            let percentage = Double(x) / 10.0
            let width = CGFloat(percentage) * containerWidth
            DispatchQueue.main.async {
                self.secondWidthContraint.constant = width
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.layoutIfNeeded()
                })
            }
            let delay = arc4random_uniform(2) + 1
            print("Second delay: \(delay)")
            sleep(delay)
        }
    }
    
    private func updateThird() {
        for x in 1...10 {
            let percentage = Double(x) / 10.0
            let width = CGFloat(percentage) * containerWidth
            DispatchQueue.main.async {
                self.thirdWidthContraint.constant = width
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.layoutIfNeeded()
                })
            }
            let delay = arc4random_uniform(2) + 1
            print("Third delay: \(delay)")
            sleep(delay)
        }
    }
}

