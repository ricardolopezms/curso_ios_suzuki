//
//  ProductListViewController.swift
//  CollectionViewExample
//
//  Created by Ricardo López on 22/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire

class ProductListViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    private var isGrid = false
    private var dataSource: [ProductModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadProducts()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        collectionView.collectionViewLayout.invalidateLayout()
//        print(view.frame.width)
//        collectionView.reloadData()
//    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ProductDetailViewController" {
            let productDetailViewController = segue.destination as! ProductDetailViewController
            productDetailViewController.product = sender as! ProductModel
        }
        else if segue.identifier == "PagerDetailViewController" {
            let pagerDetailViewController = segue.destination as! PagerDetailViewController
            pagerDetailViewController.dataSource = dataSource
            pagerDetailViewController.selectedIndex = sender as! Int
        }
    }
    
    // MARK: - User interaction
    
    @IBAction func segmentedChanged(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        isGrid = sender.selectedSegmentIndex == 0 ? false : true
        collectionView.reloadData()
    }
    
    // MARK: - Private methods
    
    private func loadProducts() {
        
        Alamofire.request(Constants.API.productsUrl)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let value):
                    print("JSON: \(value)")
                    do {
                        let productsResponse = try JSONDecoder().decode(ProductsResponseModel.self, from: value)
                        print("Productos: \(productsResponse.metadata.results.count)")
                        self.dataSource = productsResponse.metadata.results
                        self.collectionView.reloadData()
                    }
                    catch let error {
                        print("Decode error: \(error.localizedDescription)")
                    }
                    
//                    if let jsonObject = value as? [String: Any] {
//                        if let metadataDict = jsonObject["metadata"] as? [String: Any] {
//                            if let resultsArray = metadataDict["results"] as? [[String: Any]] {
//                                for productDict in resultsArray {
//                                    if let imagesArray = productDict["images"] as? [[String: Any]] {
//                                        print("Llegamos a images \(imagesArray.count)")
//                                        print(imagesArray)
//                                    }
//                                }
//                            }
//                        }
//                    }
                case .failure(let error):
                    print("Loading products error: \(error.localizedDescription)")
                }
        }
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout

extension ProductListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isGrid {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCollectionViewCell", for: indexPath) as! GridCollectionViewCell
            cell.product = dataSource[indexPath.item]
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListCollectionViewCell", for: indexPath) as! ListCollectionViewCell
            cell.product = dataSource[indexPath.item]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected: \(indexPath.item)")
        performSegue(withIdentifier: "PagerDetailViewController", sender: indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWith = view.frame.width
        if isGrid {
            let cellWidth = (screenWith - 30) / 2
            let size = CGSize(width: cellWidth, height: cellWidth)
            return size
        }
        else {
            let cellWidth = screenWith - 20
            let size = CGSize(width: cellWidth, height: 120)
            return size
        }
    }
}














