//
//  CreateTodoViewController.swift
//  TodoListExample
//
//  Created by Ricardo López on 12/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

/*
 1) Declaramos nuestro protocolo
 2) Declaramos una variable llamada 'delegate' del tipo del protocolo en la clase desde donde vamos a enviar los datos
 3) Implementamos el protocolo en la clase en la que vamos a recibir los datos
 4) Asignamos la variable 'delegate' en el prepareForSegue de la clase que va a recibir los datos
 5) Mandamos los datos a través de la variable 'delegate' desde la clase que los contiene
 */

// 1

protocol CreateTodoDelegate: class {
    func createTodoDidCreate(todoItem: TodoItem)
}

class CreateTodoViewController: BaseViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var notesTextField: UITextField!
    @IBOutlet weak var colorButton: UIButton!
    @IBOutlet weak var createButtonBottomConstraint: NSLayoutConstraint!
    
    // 2
    weak var delegate: CreateTodoDelegate?
    
    private var selectedColor = UIColor.lightGray
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manageKeyboard = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Keyboard
    
    override func keyboardWillShow(notification: Notification) {
        print("Teclado se muestra...")
        let userInfo = notification.userInfo!
        let keyboardSize = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyboardFrame = keyboardSize.cgRectValue
        
        createButtonBottomConstraint.constant = keyboardFrame.height
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func keyboardWillHide(notification: Notification) {
        print("Teclado se oculta...")
        createButtonBottomConstraint.constant = 0
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelectColorViewController" {
            let selectColorViewController = segue.destination as! SelectColorViewController
            selectColorViewController.delegate = self
        }
    }
    
    // MARK: - User interaction
    
    @IBAction func createButtonPressed(_ sender: Any) {
        
        guard let title = titleTextField.text else { return }
        guard let notes = notesTextField.text else { return }
        
        //let todoItem = TodoItem(title: title, notes: notes, done: false, color: selectedColor)
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        let todoItem = TodoItem(context: context)
        todoItem.title = title
        todoItem.notes = notes
        todoItem.done = false
        todoItem.color = selectedColor
        
        CoreDataManager.sharedInstance.saveContext()
        // 5
        delegate?.createTodoDidCreate(todoItem: todoItem)
    }
    
}

// MARK: - SelectColorDelegate

extension CreateTodoViewController: SelectColorDelegate {
    
    func selectColorDidSelect(color: UIColor) {
        print("Color recibido...")
        selectedColor = color
        colorButton.backgroundColor = color
        navigationController?.popViewController(animated: true)
    }
}
