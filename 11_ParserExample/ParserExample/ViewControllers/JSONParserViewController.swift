//
//  JSONParserViewController.swift
//  ParserExample
//
//  Created by Ricardo López on 13/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire

class JSONParserViewController: UIViewController {
    
    private var dataSource: [Book] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        loadBooks()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private methods
    
    private func loadBooks() {
        
        Alamofire.request(Constants.API.jsonURLStr)
        .validate()
            .responseData { (response) in
            
                switch response.result {
                case .success(let value):
                    print("JSON Data: \(value)")
                    do {
                        let booksResponse = try JSONDecoder().decode(BooksResponseModel.self, from: value)
                        print("Libros parseados: \(booksResponse.catalog.books.count)")
                        self.dataSource = booksResponse.catalog.books
                        
                        for book in self.dataSource {
                            print(book.title)
                            print("\t\(book.id)")
                            print("\t\(book.author)")
                            print("\t\(book.title)")
                            print("\t\(book.price)")
                            print("\t\(book.publishDate)")
                            print("\t\(book.genre)")
                            print("\t\(book.bookDescription)")
                        }
                    }
                    catch let error {
                        print("Decode error: \(error.localizedDescription)")
                    }
                case .failure(let error):
                    print("JSON request error: \(error.localizedDescription)")
                }
        }
    }

}







