//
//  XMLBookTableViewCell.swift
//  ParserExample
//
//  Created by Ricardo López on 14/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class XMLBookTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var book: Book! {
        didSet {
            print("didSet old: \(oldValue)")
            print("didSet new: \(book)")
            configureCell()
        }
        willSet {
            print("willSet \(newValue.title)")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Private methods
    
    private func configureCell() {
        titleLabel.text = book.title
        authorLabel.text = book.author
        priceLabel.text = book.price
    }
}
