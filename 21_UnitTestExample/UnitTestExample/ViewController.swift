//
//  ViewController.swift
//  UnitTestExample
//
//  Created by Ricardo López on 26/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func helloWorld() -> String {
        return "Hello world!"
    }
    
    func factorial(num: CUnsignedLongLong) -> CUnsignedLongLong {
        if num == 1 {
            return 1
        }
        sleep(1)
        return num * factorial(num: num - 1)
    }

}

