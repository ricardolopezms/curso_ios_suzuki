//
//  SearchPlacesViewController.swift
//  FourSquareExample
//
//  Created by Ricardo López on 15/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import Alamofire
import MapKit

class SearchPlacesViewController: UIViewController {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    private var userLatitude = 19.595461
    private var userLongitude = -99.206678
    private var locationManager = CLLocationManager()
    private var query = ""
    private var radius = 800

    override func viewDidLoad() {
        super.viewDidLoad()
        //searchPlaces()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        //locationManager.requestWhenInUseAuthorization()
        //locationManager.requestAlwaysAuthorization()
        //locationManager.requestLocation()
        
//        locationManager.startUpdatingLocation()
//        locationManager.stopUpdatingLocation()
        
        /*
        let annontation = MKPointAnnotation()
        let coordinate = CLLocationCoordinate2D(latitude: userLatitude, longitude: userLongitude)
        annontation.coordinate = coordinate
        annontation.title = "Suzuki Motor de México"
        mapView.addAnnotation(annontation)
        */
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        _ = checkPermissions()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VenueDetailViewController" {
            let venueDetailViewController = segue.destination as! VenueDetailViewController
            venueDetailViewController.venueAnnotation = sender as! MKAnnotation

            let userAnnotation = MKPointAnnotation()
            userAnnotation.title = "Me :D"
            let userCoordinate = CLLocationCoordinate2D(latitude: userLatitude, longitude: userLongitude)
            userAnnotation.coordinate = userCoordinate
            venueDetailViewController.userAnnotation = userAnnotation
        }
    }
    
    // MARK: - User interaction
    
    @IBAction func distanceSliderChanged(_ sender: UISlider) {
        radius = Int(sender.value)
        distanceLabel.text = "Distancia de búsqueda: \(radius)m"
    }
    
    
    // MARK: - Private methods
    
    private func checkPermissions() -> Bool {
        
        if CLLocationManager.locationServicesEnabled() {
            
            let status = CLLocationManager.authorizationStatus()
            
            if status == .authorizedWhenInUse {
                print("Podemos usar el GPS :D")
                return true
            }
            else if status == .notDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
            else if status == .restricted || status == .denied {
                print("No tenemos permiso de usar el GPS :(")
                locationServicesAlert()
            }
        }
        else {
            // TODO: Avisar al usuario que no tiene activo el servicio de localización
            locationServicesAlert()
        }
        return false
    }
    
    private func locationServicesAlert() {
        let alertController = UIAlertController(title: "Oups!", message: "Para poder usar esta aplicación, es necesario conocer tu ubicación.", preferredStyle: .alert)
        let configAction = UIAlertAction(title: "Ir a configuración", style: .default) { (action) in
            
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .destructive, handler: nil)
        alertController.addAction(configAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private func searchPlaces(query: String) {
        
        searchTextField.resignFirstResponder()
        
        let params = [
            "client_id" : Constants.FourSquareAPI.clientId,
            "client_secret" : Constants.FourSquareAPI.clientSecret,
            "v" : "20180615",
            "ll" : "\(userLatitude),\(userLongitude)",
            "query" : query,
            "radius" : "\(radius)"
        ]
        
        Alamofire.request(Constants.FourSquareAPI.baseURL, parameters: params)
            .validate()
            .responseData { (response) in
                
                switch response.result {
                case .success(let value):
                    print("Foursquare response: \(value)")
                    
                    do {
                        let foursquareResponse = try JSONDecoder().decode(FourSquareResonseModel.self, from: value)
                        print("Lugares encontrados: \(foursquareResponse.response.venues.count)")
                        self.showVenuesOnMap(venues: foursquareResponse.response.venues)
                    }
                    catch let error {
                        print("Parsing error: \(error.localizedDescription)")
                    }
                    
                case .failure(let error):
                    print("Foursquare error: \(error.localizedDescription)")
                }
        }
    }
    
    private func showVenuesOnMap(venues: [VenueModel]) {
        
        clearVenues()
        clearRoutes()
        
        for venue in venues {
            let annotation = MKPointAnnotation()
            let coordinate = CLLocationCoordinate2D(latitude: venue.location.latitude, longitude: venue.location.longitude)
            annotation.coordinate = coordinate
            annotation.title = venue.name
            mapView.addAnnotation(annotation)
        }
        
        let userCenter = CLLocationCoordinate2D(latitude: userLatitude, longitude: userLongitude)
        let region = MKCoordinateRegionMakeWithDistance(userCenter, Double(radius + 400), Double(radius + 200))
        mapView.setRegion(region, animated: true)
    }
    
    private func clearVenues() {
        let currentAnnotations = mapView.annotations
        mapView.removeAnnotations(currentAnnotations)
    }
    
    private func loadRouteFor(annotation: MKAnnotation) {
        
        clearRoutes()
        
        let placeMark = MKPlacemark(coordinate: annotation.coordinate)
        let destinationMapItem = MKMapItem(placemark: placeMark)
        
        let request = MKDirectionsRequest()
        request.transportType = .walking
        request.requestsAlternateRoutes = false
        request.source = MKMapItem.forCurrentLocation()
        request.destination = destinationMapItem
        
        let directions = MKDirections(request: request)
        directions.calculate { (response, error) in
            
            if let error = error {
                print("Directions error: \(error.localizedDescription)")
            }
            else {
                if let routes = response?.routes {
                    for route in routes {
                        for step in route.steps {
                            print(step.instructions)
                        }
                        // TODO: Agregar la ruta al mapa
                        self.mapView.add(route.polyline, level: .aboveRoads)
                    }
                }
            }
        }
    }
    
    private func clearRoutes() {
        
        let currentOverlays = mapView.overlays
        mapView.removeOverlays(currentOverlays)
    }
}

// MARK: - UITextFieldDelegate

extension SearchPlacesViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("Enter pressed...")
        if checkPermissions() {
            if let searchQuery = textField.text {
                print("Buscar: \(searchQuery)")
                query = searchQuery
                locationManager.requestLocation()
                //searchPlaces(query: searchQuery)
            }
        }
        return true
    }
}

// MARK: - CLLocationManagerDelegate

extension SearchPlacesViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Localización del usuario encontrada...")
        if let location = locations.first {
            userLatitude = location.coordinate.latitude
            userLongitude = location.coordinate.longitude
            searchPlaces(query: query)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("LocationManager error: \(error.localizedDescription)")
    }
}

// MARK: - MKMapViewDelegate

extension SearchPlacesViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }
        
        let annotationIdentifier = "VenueAnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.canShowCallout = true
        }
        else {
            annotationView?.annotation = annotation
        }
        
        annotationView?.image = #imageLiteral(resourceName: "pin_blue")
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("Punto seleccionado...")
        if let annotation = view.annotation {
            //loadRouteFor(annotation: annotation)
            performSegue(withIdentifier: "VenueDetailViewController", sender: annotation)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .red
        renderer.lineWidth = 5
        renderer.alpha = 0.75
        return renderer
    }
}





