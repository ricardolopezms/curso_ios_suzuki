//
//  ViewController.swift
//  ClasesYMetodos
//
//  Created by Ricardo López on 04/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print("¡Hola mundo!")
        
        // Constante implícita tipo Int
        let x = 0
        // Constante implícita tipo String
        let name = "Ricardo"
        
        // Constante explícita tipo Int
        let y: Int = 1
        // Constante explícita tipo String
        let lastName: String = "López"
        
        print(x)
        print(name)
        print("\(x) - \(y)")
        print("Nombre completo: \(name) \(lastName)")
        
        // Variable implícita tipo Int
        var x1 = 0
        // Variable explícita tipo Int
        var y1: Int = 1
        
        print("+++++ ARREGLOS +++++")
        
        let myArray = [1, 2, 3, 4, 5]
        let myArray2: [Int] = [1, 2, 3]
        let myArray3: Array<Int> = Array()
        let myArray4: [Int] = []
        let myArray5: [Any] = [1, 2.3, "Hola"]
        
        print(myArray)
        print(myArray5)
        
        // For each
        for number in myArray {
            print("Número: \(number)")
        }
        
        // For tradicional
        // 0..<MAX -> x < MAX
        // 0...MAX -> x <= MAX
        for x in 0..<myArray.count {
            print("Número [\(x)] -> \(myArray[x])")
        }
        
        print("+++++ DICCIONARIOS +++++")
        
        let myDictionary = [
            "key" : "value",
            "name" : "Ricardo",
            "last_name" : "López"
        ]
        
        print("Mi diccionario: \(myDictionary)")
        print("Name: \(myDictionary["name"])")
        print("Una llave que no existe: \(myDictionary["asd"])")
        
        if let name = myDictionary["name"] {
            // La llave name existe y tiene un valor
            print("Name existe: \(name)")
        }
        else {
            // La llave name NO existe y es nil
            print("Name no existe")
        }
        
        if let asd = myDictionary["asd"] {
            // La llave name existe y tiene un valor
            print("asd existe: \(asd)")
        }
        else {
            // La llave name NO existe y es nil
            print("asd NO existe")
        }
        
        //var myDictionary3: Dictionary<String, Int> = Dictionary()
        var myDictionary2: [String: Int] = [:]
        myDictionary2["uno"] = 1
        myDictionary2["dos"] = 2
        myDictionary2["tres"] = 3
        
        // Obtener las llaves del diccionario
        let keysArray = Array(myDictionary.keys)
        // Obtener los valores del diccionario
        let valuesArray = Array(myDictionary.values)
        
        print(keysArray)
        print(valuesArray)
        
        // Iteramos llaves y valores
        for (key, value) in myDictionary {
            print("Llave: \(key) -> Valor: \(value)")
        }
        
        let stringOptional: String! = nil
        let stringOptionalTwo: String = "Testing optionals"
        
        print(stringOptional)
        print(stringOptionalTwo)
        
        print("+++++ FUNCIONES +++++")
        
        helloWorld()
        let sumResult = suma(numA: 6, numB: 5)
        print("Suma: \(sumResult)")
        
        let sumResult2 = sumaSinLabels(7, 8)
        print("Suma2: \(sumResult2)")
        
        let sumResult3 = sumaConLabelDiferente(x: 4, y: 3)
        print("Suma3: \(sumResult3)")
        
        let resResult = resta(numA: 5, numB: 4)
        print("Resta: \(resResult)")
        
        let resResult2 = resta(numA: 7, numB: 2, numC: 2)
        print("Resta2: \(resResult2)")
        
        let multiples = multiplesValores()
        print("\(multiples.0) \(multiples.1) \(multiples.2)")
        
        let multiples2 = multiplesValoresV2()
        print("\(multiples2.age) \(multiples2.name) \(multiples2.pi)")
        
        print("+++++ CLASES +++++")
        let ricardo = Person(name: "Ricardo", age: 30)
        ricardo.sayHello()
//        ricardo.name = "Ricardo"
//        ricardo.age = 30
        
        let ricardoDeveloper = Developer(name: "Ricardo López", age: 30, programmingLanguage: "Swift 4")
        ricardoDeveloper.sayHello()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Private methods
    
    private func helloWorld() {
        print("Hola mundo :D")
    }

    private func suma(numA: Int, numB: Int) -> Int {
        return numA + numB
    }
    
    private func sumaSinLabels(_ numA: Int, _ numB: Int) -> Int {
        return numA + numB
    }
    
    private func sumaConLabelDiferente(x numA: Int, y numB: Int) -> Int {
        return numA + numB
    }
    
    private func resta(numA: Int, numB: Int, numC: Int? = nil) -> Int {
        if let numC = numC {
            return numA - numB - numC
        }
        return numA - numB
    }
    
    private func multiplesValores() -> (Int, String, Double) {
        return (12, "Hola", 2.1)
    }
    
    private func multiplesValoresV2() -> (age: Int, name: String, pi: Double) {
        return (30, "Ricardo", 3.1416)
    }
}









