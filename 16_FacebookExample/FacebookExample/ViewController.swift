//
//  ViewController.swift
//  FacebookExample
//
//  Created by Ricardo López on 21/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        /*
        let loginButton = LoginButton(readPermissions: [.email, .publicProfile, .userFriends])
        loginButton.center = view.center
        view.addSubview(loginButton)
         */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func facebookLoginButtonPressed(_ sender: Any) {
        
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.email, .publicProfile, .userFriends], viewController: self) { (loginResult) in
            
            switch loginResult {
            case .failed(let error):
                print("Login error: \(error.localizedDescription)")
            case .cancelled:
                print("User has cancelled login")
            case .success(let grantedPermissions, let declinedPermissions, let token):
                print("Granted: \(grantedPermissions)")
                print("Declined: \(declinedPermissions)")
                print("Access token: \(token.authenticationToken)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SHOW_PROFILE_SCREEN"), object: nil)
            }
        }
    }
}







