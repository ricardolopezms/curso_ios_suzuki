//
//  VisualFormatLanguageViewController.swift
//  ScrollViewExample
//
//  Created by Ricardo López on 08/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class VisualFormatLanguageViewController: UIViewController {

    @IBOutlet weak var contentScrollView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        let redView = UIView()
//        redView.translatesAutoresizingMaskIntoConstraints = false
//        redView.backgroundColor = .red
//        contentScrollView.addSubview(redView)
//
//        let horizontalFormat = "H:|-[redView]-|"
//        let verticalFormat = "V:|-[redView(1000)]-|"
//
//        let views = [
//            "redView" : redView
//        ]
//
//        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalFormat, options: .alignAllFirstBaseline, metrics: nil, views: views)
//        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: verticalFormat, options: .alignAllFirstBaseline, metrics: nil, views: views)
//
//        contentScrollView.addConstraints(horizontalConstraints)
//        contentScrollView.addConstraints(verticalConstraints)
        
        createViews(numberOfViews: 10)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private methods
    
    private func createViews(numberOfViews: Int) {
        
        var verticalViews: [String: UIView] = [:]
        var verticalElements: [String] = []
        var metrics: [String: Int] = [:]
        
        for x in 0..<numberOfViews {
            print("Creamos view[\(x)]")
            
            let view = UIView()
            
            let red = CGFloat(arc4random() % 255)
            let green = CGFloat(arc4random() % 255)
            let blue = CGFloat(arc4random() % 255)
            view.backgroundColor = UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1)
            
            //view.backgroundColor = .blue
            view.translatesAutoresizingMaskIntoConstraints = false
            contentScrollView.addSubview(view)
            
            // Creamos el identificador único del View
            let viewIdenfier = "view\(x)"
            // Añadimos el View al diccionario con su identificador
            verticalViews[viewIdenfier] = view
            // Guardamos el identificador para las constraints verticales
            verticalElements.append("[\(viewIdenfier)(ALTURA\(x))]")
            metrics["ALTURA\(x)"] = Int(arc4random() % 150)
            // Creamos el formato de constraints horizontales
            let horizontalFormat = "H:|-[\(viewIdenfier)]-|"
            // Creamos las constraints
            let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: horizontalFormat, options: .alignAllFirstBaseline, metrics: nil, views: verticalViews)
            contentScrollView.addConstraints(horizontalConstraints)
        }
        
        let verticalFormat = "V:|-\(verticalElements.joined(separator: "-"))-|"
        print(verticalFormat)
        
//        let metrics = [
//            "ALTURA" : 300
//        ]
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: verticalFormat, options: .alignAllLeft, metrics: metrics, views: verticalViews)
        contentScrollView.addConstraints(verticalConstraints)
    }

}






