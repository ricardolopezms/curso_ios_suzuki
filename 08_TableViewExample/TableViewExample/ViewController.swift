//
//  ViewController.swift
//  TableViewExample
//
//  Created by Ricardo López on 08/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    private var planetsArray: [Planet] = []
    private var studentsArray: [Student] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        planetsArray.append(Planet(name: "Mercurio", planetDescription: "Descripción de Mercurio"))
        planetsArray.append(Planet(name: "Venus", planetDescription: "Descripción de Venus"))
        planetsArray.append(Planet(name: "Tierra", planetDescription: "Descripción de Tierra"))
        planetsArray.append(Planet(name: "Marte", planetDescription: "Descripción de Marte"))
        planetsArray.append(Planet(name: "Júpiter", planetDescription: "Descripción de Júpiter"))
        planetsArray.append(Planet(name: "Saturno", planetDescription: "Descripción de Saturno"))
        planetsArray.append(Planet(name: "Urano", planetDescription: "Descripción de Urano"))
        planetsArray.append(Planet(name: "Neptuno", planetDescription: "Descripción de Neptuno"))
        planetsArray.append(Planet(name: "Plutón", planetDescription: "Descripción de Plutón"))
        
        studentsArray.append(Student(name: "Ricardo López", programmingLanguage: "Swift 4 & Java"))
        studentsArray.append(Student(name: "Daniel Barrera", programmingLanguage: "Swift 4 & C#"))
        studentsArray.append(Student(name: "Gabriel Gómez", programmingLanguage: "Swift 4.1 & C#"))
        studentsArray.append(Student(name: "Ángel Pérez", programmingLanguage: "Swift & C#"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlanetDetailViewController" {
            let planetDetailViewController = segue.destination as! PlanetDetailViewController
            planetDetailViewController.planet = sender as! Planet
        }
    }
    
    // MARK: - User interaction
    
    @IBAction func editButtonPressed(_ sender: Any) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        editButton.title = tableView.isEditing ? "Listo" : "Editar"
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return planetsArray.count
        }
        return studentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlanetTableViewCell")!
            let planet = planetsArray[indexPath.row]
            cell.textLabel?.text = planet.name
            cell.accessoryType = .disclosureIndicator
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "StudentTableViewCell")!
            let student = studentsArray[indexPath.row]
            cell.textLabel?.text = student.name
            cell.detailTextLabel?.text = student.programmingLanguage
            cell.accessoryType = .checkmark
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Planetas"
        }
        return "Estudiantes"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Celda seleccionada \(indexPath.row)")
        if indexPath.section == 0 {
            let planet = planetsArray[indexPath.row]
            performSegue(withIdentifier: "PlanetDetailViewController", sender: planet)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Table deleting
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            print("Usuario quiere eliminar celda...")
            studentsArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "😱"
    }
    
    /*
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let action = UITableViewRowAction(style: .normal, title: "Compartir") { (action, indexPath) in
            print("Seleccionó compartir")
        }
        
        let actionTwo = UITableViewRowAction(style: .destructive, title: "Eliminar") { (action, indexPath) in
            print("Selenccionó eliminar")
            self.studentsArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        return [actionTwo, action]
    }
    */
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let studentToMove = studentsArray[sourceIndexPath.row]
        studentsArray.remove(at: sourceIndexPath.row)
        studentsArray.insert(studentToMove, at: destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        
        if sourceIndexPath.section != proposedDestinationIndexPath.section {
            return sourceIndexPath
        }
        return proposedDestinationIndexPath
    }
}










