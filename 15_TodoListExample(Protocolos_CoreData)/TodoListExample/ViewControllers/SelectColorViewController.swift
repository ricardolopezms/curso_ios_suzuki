//
//  SelectColorViewController.swift
//  TodoListExample
//
//  Created by Ricardo López on 13/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

protocol SelectColorDelegate: class {
    func selectColorDidSelect(color: UIColor)
}

class SelectColorViewController: BaseViewController {

    weak var delegate: SelectColorDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - User interaction
    
    @IBAction func colorButtonPressed(_ sender: UIButton) {
        guard let color = sender.backgroundColor else { return }
        delegate?.selectColorDidSelect(color: color)
    }
}
