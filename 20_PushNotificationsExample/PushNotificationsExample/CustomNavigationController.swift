//
//  CustomNavigationController.swift
//  PushNotificationsExample
//
//  Created by Ricardo López on 27/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(showDocument(notification:)), name: NSNotification.Name(rawValue: "OPEN_DOCUMENT"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private methods
    
    @objc private func showDocument(notification: Notification) {
        
        if let params = notification.object as? [String: String] {
            
            let documentType = params["document_type"]!
            let folio = params["folio"]!
            
            switch documentType {
            case "1":
                let viewController = UIViewController()
                viewController.view.backgroundColor = .red
                pushViewController(viewController, animated: true)
            case "2":
                let viewController = UIViewController()
                viewController.view.backgroundColor = .green
                pushViewController(viewController, animated: true)
            case "3":
                let viewController = UIViewController()
                viewController.view.backgroundColor = .blue
                pushViewController(viewController, animated: true)
            default:
                print("Unkown document type")
            }
        }
    }
}












