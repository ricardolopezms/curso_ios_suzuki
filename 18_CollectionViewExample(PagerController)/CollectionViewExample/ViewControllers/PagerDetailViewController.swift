//
//  PagerDetailViewController.swift
//  CollectionViewExample
//
//  Created by Ricardo López on 25/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class PagerDetailViewController: UIViewController {
    
    private var pageViewController: UIPageViewController!
    
    var dataSource: [ProductModel] = []
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageViewController = storyboard?.instantiateViewController(withIdentifier: "PageViewController") as! UIPageViewController
        pageViewController.dataSource = self
        
        addChildViewController(pageViewController)
        view.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
        
        print("Products: \(dataSource.count)")
        
        let firstViewController = createViewControllerAt(index: selectedIndex)
        pageViewController.setViewControllers([firstViewController], direction: .forward, animated: false, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Private methods
    
    private func createViewControllerAt(index: Int) -> UIViewController {
        let productDetailViewController = storyboard?.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        productDetailViewController.product = dataSource[index]
        productDetailViewController.pageIndex = index
        return productDetailViewController
    }
}

// MARK: - UIPageViewControllerDataSource

extension PagerDetailViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let currentProductDetailViewController = viewController as? ProductDetailViewController {
            let currentIndex = currentProductDetailViewController.pageIndex
            if currentIndex == 0 {
                return nil
            }
            else {
                let productDetailViewController = createViewControllerAt(index: currentIndex - 1)
                return productDetailViewController
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let currentProductDetailViewController = viewController as? ProductDetailViewController {
            let currentIndex = currentProductDetailViewController.pageIndex
            if currentIndex == dataSource.count - 1 {
                return nil
            }
            else {
                let productDetailViewController = createViewControllerAt(index: currentIndex + 1)
                return productDetailViewController
            }
        }
        return nil
    }
}





