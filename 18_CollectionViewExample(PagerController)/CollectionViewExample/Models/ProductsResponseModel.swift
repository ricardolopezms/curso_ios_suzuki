//
//  ProductsResponseModel.swift
//  CollectionViewExample
//
//  Created by Ricardo López on 18/01/18.
//  Copyright © 2018 Ricardo López. All rights reserved.
//

import Foundation

class ProductsResponseModel: Codable {
    var metadata: ProductsMetadataModel
}

