//
//  ConfigurationViewController.swift
//  TodoListExample
//
//  Created by Ricardo López on 12/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class ConfigurationViewController: BaseViewController {

    @IBOutlet weak var detailSwicth: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let isDetailActive = UserDefaults.standard.bool(forKey: "DETAIL_ACTIVE")
        detailSwicth.setOn(isDetailActive, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - User interaction
    
    @IBAction func detailSwicthChanged(_ sender: UISwitch) {
        print("Cambió...")
        UserDefaults.standard.set(sender.isOn, forKey: "DETAIL_ACTIVE")
        UserDefaults.standard.synchronize()
    }
}





