//
//  UserProfileViewController.swift
//  FacebookExample
//
//  Created by Ricardo López on 21/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import AlamofireImage

class UserProfileViewController: UIViewController {

    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userPhotoImageView.layer.cornerRadius = userPhotoImageView.frame.width / 2
        userPhotoImageView.layer.borderColor = UIColor.orange.cgColor
        userPhotoImageView.layer.borderWidth = 2
        loadUserInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - User interaction
    
    @IBAction func logOutButtonPressed(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logOut()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SHOW_LOGIN_SCREEN"), object: nil)
    }
    
    // MARK: - Private methods
    
    private func loadUserInfo() {
        
        let connection = GraphRequestConnection()
        let params = [
            "fields" : "id, name, picture.width(400).height(400)"
        ]
        let request = GraphRequest(graphPath: "/me", parameters: params)
        
        connection.add(request) { (response, result) in
            
            switch result {
            case .success(let response):
//                print(response.dictionaryValue)
//                print(response.stringValue)
                do {
                    let data = try JSONSerialization.data(withJSONObject: response.dictionaryValue!, options: [])
                    let facebookResponse = try JSONDecoder().decode(FacebookResponseModel.self, from: data)
                    
                    self.userNameLabel.text = facebookResponse.name
                    
                    if let imageURL = URL(string: facebookResponse.picture.data.url) {
                        self.userPhotoImageView.af_setImage(withURL: imageURL)
                    }
                }
                catch let error {
                    print("Parsing error: \(error.localizedDescription)")
                }
                
            case .failed(let error):
                print("Error: \(error.localizedDescription)")
            }
        }
        connection.start()
    }
}








