//
//  CampaniasRouter.swift
//  SOAPExample
//
//  Created by Ricardo López on 15/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation
import Alamofire

enum CampaniasRouter: URLRequestConvertible {
    
    //static let validarCampaniasPath = "/WSLogica/WSLogicaDMS.asmx?WSDL"
    
    case validarCampania(model: ValidarCampaniasRequestModel)
    case validarCampaniaGR(model: GeneralRequestModel)
    
    func asURLRequest() throws -> URLRequest {
        
        let baseURLString = "http://189.206.77.234:88/WSLogica/WSLogicaDMS.asmx?WSDL"
        let url = URL(string: baseURLString)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("text/xml", forHTTPHeaderField: "Content-Type")
        
        switch self {
        case .validarCampania(let model):
            urlRequest.httpBody = model.requestBody
        case .validarCampaniaGR(let model):
            urlRequest.httpBody = model.requestBody
        }
        
        return urlRequest
    }
}

class ValidarCampaniasRequestModel {
    
    var dealer = ""
    var vin = ""
    
    init(dealer: String, vin: String) {
        self.dealer = dealer
        self.vin = vin
    }
    
    var requestBody: Data {
        let xmlRequestBody = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<soap:Body>" +
            "<ValidarCampanias xmlns=\"http://suzuki.com.mx/\">" +
            "<_vin>\(vin)</_vin>" +
            "<_idDealer>\(dealer)</_idDealer>" +
            "</ValidarCampanias>" +
            "</soap:Body>" +
        "</soap:Envelope>"
        return xmlRequestBody.data(using: .utf8)!
    }
}

class GeneralRequestModel {
    
    var xmlString = ""
    
    init(operationName: String, properties: [String: String]) {
        
        var propertiesStr = ""
        for (key, value) in properties {
            let propertyLine = "<\(key)>\(value)</\(key)>"
            propertiesStr += propertyLine
        }
        
        let finalBody = "<\(operationName) xmlns=\"http://suzuki.com.mx/\">\(propertiesStr)</\(operationName)>"
     
        xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            "<soap:Body>" +
            finalBody +
            "</soap:Body>" +
        "</soap:Envelope>"
    }

    var requestBody: Data {
        return xmlString.data(using: .utf8)!
    }
}





