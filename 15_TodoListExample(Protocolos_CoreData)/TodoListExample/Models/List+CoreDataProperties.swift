//
//  List+CoreDataProperties.swift
//  TodoListExample
//
//  Created by Ricardo López on 20/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//
//

import Foundation
import CoreData


extension List {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<List> {
        return NSFetchRequest<List>(entityName: "List")
    }

    @NSManaged public var name: String?
    @NSManaged public var todos: NSSet?

}

// MARK: Generated accessors for todos
extension List {

    @objc(addTodosObject:)
    @NSManaged public func addToTodos(_ value: TodoItem)

    @objc(removeTodosObject:)
    @NSManaged public func removeFromTodos(_ value: TodoItem)

    @objc(addTodos:)
    @NSManaged public func addToTodos(_ values: NSSet)

    @objc(removeTodos:)
    @NSManaged public func removeFromTodos(_ values: NSSet)

}
