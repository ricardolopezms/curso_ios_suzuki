//
//  NewsDetailViewController.swift
//  ScrollViewExample
//
//  Created by Ricardo López on 08/06/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = "Título de la noticia aquí Título de la noticia aquí Título de la noticia aquí Título de la noticia aquí"
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd 'de' MMM',' YYYY 'a las' HH:mm"
        //dateLabel.text = "08 de Junio, 2018"
        dateLabel.text = dateFormatter.string(from: date)
        textLabel.text = "El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí v El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí El texto de la noticia importante debe aparecer aquí"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
